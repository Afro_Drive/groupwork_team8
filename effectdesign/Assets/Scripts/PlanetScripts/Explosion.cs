﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 特定回数公転軌道に入ると爆発する処理
/// 作成者:
/// </summary>
public class Explosion : MonoBehaviour
{
    private GameObject Player;
    private int ExplodCnt;
    private int NowCnt;

    private int TimeCnt;

    // Start is called before the first frame update
    void Start()
    {
        ExplodCnt = 2;
        NowCnt = 0;
        TimeCnt = 120;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Player = other.gameObject;
            NowCnt += 1;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (NowCnt >= ExplodCnt)
        {
            Player.GetComponent<Rotation>().Explode();
            Player.transform.position -= (transform.position - Player.transform.position).normalized / (Vector3.Distance(transform.position, Player.transform.position) * (Vector3.Distance(transform.position, Player.transform.position))) * 150;
            TimeCnt -= 1;

            if (TimeCnt == 0)
            {
                Destroy(gameObject);
            }
        }
    }
}
