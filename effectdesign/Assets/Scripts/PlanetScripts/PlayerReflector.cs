﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// コースの外の壁
/// </summary>
public class PlayerReflector : MonoBehaviour
{
    /// <summary>
    /// コライダー接触時の処理
    /// （ColliderのIsTriggerにチェックが入った場合のみ処理）
    /// </summary>
    /// <param name="other">コライダー内に侵入したオブジェクト</param>
    private void OnTriggerEnter(Collider other)
    {
        //Playerが接触したら
        if (other.CompareTag("Player"))
        {
            //コースアウト処理
            other.GetComponent<PlayerMover>().CorseOut();
        }
    }
}
