﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialGrav : MonoBehaviour
{
    private bool istouch;//ぶつかったかどうか
    private bool downspeed;//減速用の変数
    private Transform player;

    private Collider hit;
    // Start is called before the first frame update
    void Start()
    {
        hit = null;
        istouch = false;
        downspeed = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(downspeed==true)
        {
            SpeedDown();
            downspeed = false;
        }
    }
    private void OnTriggerEnter(Collider hit)
    {
        this.hit = hit;
        if (this.hit.CompareTag("Player"))
        {
            
            player = GameObject.FindWithTag("Player").transform;
            if (istouch==false)
            {
                istouch = true;
                downspeed = true;
            }
            
        }
    }
    void SpeedDown()
    {
        player.GetComponent<Rotation>().DownSpeedtoGrav();
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space)
            || Input.GetButtonDown("Fire1"))
        {
            Start();
        }
    }
}
