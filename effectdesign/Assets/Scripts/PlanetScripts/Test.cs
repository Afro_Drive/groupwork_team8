﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オブジェクト本体を回転させることで、
/// 公転軌道にいるPlayerの公転角度を動的に変化させる処理
/// 
/// (オブジェクトから見たPlayerの回転軌道は変わっていないが、
/// オブジェクト自体が回転しているため、ゲーム画面では、
/// あたかもPlayerが3次元的に自在に回転しているように見せている)
/// 
/// 作成者:翔ちゃん
/// </summary>
public class Test : MonoBehaviour
{
    //フィールド
    //X方向のキー入力量
    float x = 0;
    //Y方向のキー入力量
    float y = 0;

    private float RotationX;
    private float RotationY;
    //惑星の衛星軌道にPlayerが合流しているか？
    bool iscatch;
    //時間切れか？
    bool isTime;
    //なんかのタイマー
    float timecount;
    //自身のコライダー
    private Collider hit;

    private Transform rotationStar;

    private float speedpenalty;

    public float revisionParam = 15f;

    public float rotationControl = 0.25f;

    // Start is called before the first frame update
    void Start()
    {
        //コライダーは空っぽで初期化
        hit = null;
        //惑星の公転軌道からは離脱状態にする
        RotationStop();
        //タイマーはOFF
        isTime = false;
        //タイマーの残り時間は2で初期化(2がマジックナンバー)
        timecount = 2;
    }

    // Update is called once per frame
    void Update()
    {
        
        RotationY = Mathf.Abs((90-transform.localEulerAngles.y % 180)/90);
        RotationX = Mathf.Abs((90 - transform.localEulerAngles.x % 180) / 90);
        rotationStar = GameObject.FindWithTag("Player").transform;
         float nSpeed =
                (rotationStar.GetComponent<PlayerMover>().
                Velocity.
                magnitude) * revisionParam;
        
        speedpenalty = rotationStar.GetComponent<Rotation>().SpeedPenalty();
        //Debug.Log(nSpeed);
        //自身を回転させる
        transform.Rotate(new Vector3(1, 0, 0), -x*nSpeed*rotationControl*speedpenalty, Space.Self);//X方向に回転
        transform.Rotate(new Vector3(0, 1, 0), -y*nSpeed * rotationControl*speedpenalty, Space.Self);//Y方向に回転
        //タイマー起動状態なら
        if (isTime)
        {
            //タイマーの残り時間を減算
            timecount = timecount - 1;
        }
        //タイマーの残り時間がなくなった
        if (timecount == 0)
        {
            //初期化(角度変化などを⓪に戻す)
            Initialize();
            //タイマーをOFFにする
            isTime = false;
            //タイマーの残り時間を初期化(2がマジックナンバー)
            timecount = 1;
        }
    }

    /// <summary>
    /// フレーム数に影響されず一定間隔で処理される更新処理
    /// </summary>
    void FixedUpdate()
    {
        //惑星の衛星軌道にPlayerがいるなら
        if(iscatch)
        {
            //X,Y方向のキー入力を取得
            
                y = Input.GetAxis("Horizontal");
                x = Input.GetAxis("Vertical");

            

            //スペースキーかゲームパッドのFire1ボタンが入力された
            if (GameObject.Find("Player").transform.IsChildOf(transform)==false)
            {
                //Playerが衛星軌道から離脱状態にする
                RotationStop();
                //タイマーをONにする
                isTime = true;
            }
        }



    }

    /// <summary>
    /// 自身のコライダー内にオブジェクトが侵入してきた際の処理
    /// (自身のColliderコンポーネントのIsTriggerにチェックが入っている処理される)
    /// </summary>
    /// <param name="hit">衝突対象</param>
    void OnTriggerEnter(Collider hit)
    {
        //引数受け取り
        this.hit = hit;
        //衝突対象の親を自身にする
        //this.hit.transform.parent = transform;
        // transform.Rotate(new Vector3(1, 0, 0), Mathf.Atan2((hit.transform.position.z - transform.position.z), (hit.transform.position.x - transform.position.x)) * 180.0f / Mathf.PI, Space.Self);
        //衝突対象の所有タグがPlayerなら
        if (this.hit.CompareTag("Player"))
        {
            //   transform.Rotate(new Vector3(0, 0, 1), Mathf.Atan2((transform.position.y - hit.transform.position.y), (transform.position.x - hit.transform.position.x)) * 180.0f / Mathf.PI, Space.Self);
            //自身の衛星軌道内に合流させる
            RotationStart();
        }
    }

    /// <summary>
    /// 角度の初期化
    /// </summary>
    void Initialize()
    {
        //キー入力による回転角度を０に戻す
        x = 0;
        y = 0;
        //自身の回転を０に戻す
        transform.rotation = Quaternion.identity;
    }

    public float GetRotationY()
    {
        return RotationY;
    }
    public float GetRotationX()
    {
        return RotationX;
    }
    private void RotationStart()
    {
        iscatch = true;
        Debug.Log("RotationStart");
    }
    private void RotationStop()
    {
        iscatch = false;
        Debug.Log("RotationStop");
    }
}
