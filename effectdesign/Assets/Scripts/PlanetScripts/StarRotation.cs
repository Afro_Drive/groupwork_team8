﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 衛星を所有でき、自らも公転する惑星
/// 作成者:
/// </summary>
public class StarRotation : MonoBehaviour
{
    float nDegree = 0.0f; // 度数値
public float nSpeed = 2.5f; // オブジェクトの移動するスピード
float nDegreeToRadian = Mathf.PI / 180.0f; // nDegreeの値をラジアンに変換する
float nRadian; // nDegreeの値をラジアンに変換した値
    public float nCenterX = 0.0f; // 楕円運動の水平中心座標
    public float nCenterZ = 0.0f; // 楕円運動の垂直中心座標
    public float nRadiusX = 16.0f; // 楕円運動の水平方向の半径
    public float nRadiusZ = 5.0f; // 楕円運動の垂直方向の半径


　　
　　// Use this for initialization

　　void Start () 

　　{


　　}

　　// Update is called once per frame

　　void Update () 
　　{
        nDegree += nSpeed; // 移動する分の値をnDegreeに入れる
        nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
        nRadian = nDegree * nDegreeToRadian;// nDegreeの値をラジアンに変換した値をnRadianに入れる

        transform.position = new Vector3(nCenterX + Mathf.Cos(nRadian) * nRadiusX, 5, nCenterZ + Mathf.Sin(nRadian) * nRadiusZ);

　　}

}
