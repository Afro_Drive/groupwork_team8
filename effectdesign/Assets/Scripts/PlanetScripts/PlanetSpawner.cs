﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour
{
    public GameObject planet;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            
            Instantiate
                (planet,
                new Vector3(transform.position.x + 20, transform.position.y, transform.position.z + 20)
                , transform.rotation);
            
        }
    }
    
}
