﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ブラックホール
/// (一定範囲に入ると周りを回転しつつ引き寄せる)
/// このスクリプトは実際に入れ込むものではないですが、下書きとして使います。
/// 作成者:
/// </summary>
public class GravityArea : MonoBehaviour
{
    public float gravity; //引き寄せの強さ
    public float angle; //回転角度
    private Collider player; //回すPlayer
    private Transform gravplayer;
    //private bool isgraved;

    // Start is called before the first frame update
    void Start()
    {
        //isgraved=false;
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.tag != "Player")
        {
            return;
        }
       player = collision;//ここの処理を、
        //isgraved=true;
    }

    private void OnTriggerExit(Collider other)
    {
        //ifでGravityAreaの条件分け
        player = null;
        //isgraved=false;にする。
    }

    private void Update()
    {
        if (player == null)
        {
            return;
        }
        player.transform.RotateAround(transform.position, Vector3.up, angle);
        player.transform.position -= ((player.transform.position - transform.position).normalized) / 10 * gravity;
        
    }

    //private void GravCatch()
//    {
          //if(isgraved&&IsCatch==false)重力の影響を受けつつ、公転軌道に乗っていないときのみ。
          //{
          //ここに上のUpdate内の内容を記述。
          //んで、本家のUpdate内にGraveCatch()を実装。
          //}
//    }
}
