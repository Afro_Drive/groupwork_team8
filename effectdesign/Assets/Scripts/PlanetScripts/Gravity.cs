﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 自身に向けて徐々に引き寄せる処理
/// 作成者:
/// </summary>
public class Gravity : MonoBehaviour
{

    Collider hit;
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.hit = other;
        }
    }

    void FixedUpdate()
    {
        // 星に向かう向きの取得
        var direction = transform.position - hit.transform.position;
        hit.transform.position = hit.transform.position + direction/75;
    }
}
