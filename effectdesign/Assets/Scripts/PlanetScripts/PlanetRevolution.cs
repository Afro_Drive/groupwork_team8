﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 公転する惑星(XY平面、XZ平面を選択可能)
/// </summary>
public class PlanetRevolution : MonoBehaviour
{
    //フィールド
    float nDegree = 0.0f; // 度数値
    public float nSpeed = 20.0f; // オブジェクトの移動するスピード
    readonly float nDegreeToRadian = Mathf.PI / 180.0f; // nDegreeの値をラジアンに変換する
    float nRadian; // nDegreeの値をラジアンに変換した値
    public float nCenterX = 0.0f; // 楕円運動の水平中心座標
    public float nCenterZ = 0.0f; // 楕円運動の垂直中心座標
    float nRadiusX = 10.0f; // 楕円運動の水平方向の半径
    float nRadiusZ = 10.0f; // 楕円運動の垂直方向の半径
    [SerializeField, Header("公転の中心オブジェクト")]
    private GameObject Point;
    [SerializeField]
    private float angle;//これ使われてないね。nSpeedと役割が重複してる？

    private GameObject player;


    [SerializeField, Header("回転をXY平面にするか")]
    bool rotateXY;

    /// <summary>
    /// 公転角度の設定
    /// </summary>
    void Set()
    {
        //自分と公転の中心のオブジェクトとの距離を測定
        nRadiusX = Mathf.Sqrt(((transform.position.x - Point.transform.position.x) * (transform.position.x - Point.transform.position.x)) +
           ((transform.position.z - Point.transform.position.z) * (transform.position.z - Point.transform.position.z)));
        nRadiusZ = nRadiusX;
    }

    /// <summary>
    /// 毎フレームの更新
    /// </summary>
    void Update()
    {
        //XY平面を公転するなら
        if (rotateXY)
        {
            //XY平面用の角度の設定
            SetXY();
            //XY平面を回転する
            RotateXY();
        }
        else//XZ平面を回転するなら
        {
            Set();

            //ここの処理はRotateXZとしてメソッド化すると見やすくなりそう
            nDegree = nDegree + nSpeed; // 移動する分の値をnDegreeに入れる
            nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
            nRadian = nDegree * nDegreeToRadian; // nDegreeの値をラジアンに変換した値をnRadianに入れる

            //XZ平面上を回転する
            transform.position
                = new Vector3(Point.gameObject.transform.position.x + Mathf.Cos(nRadian) * nRadiusX,
                Point.gameObject.transform.position.y,
                Point.gameObject.transform.position.z + Mathf.Sin(nRadian) * nRadiusZ);
        }

        if (player == null)
            return;
        
    }

    /// <summary>
    /// XY平面での回転角度の設定
    /// </summary>
    private void SetXY()
    {
        nRadiusX = Mathf.Sqrt(((transform.position.x - Point.transform.position.x) * (transform.position.x - Point.transform.position.x)) +
           ((transform.position.y - Point.transform.position.y) * (transform.position.y - Point.transform.position.y)));
        nRadiusZ = nRadiusX;
    }

    /// <summary>
    /// XY平面を公転する
    /// </summary>
    private void RotateXY()
    {
        nDegree = nDegree + nSpeed; // 移動する分の値をnDegreeに入れる
        nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
        nRadian = nDegree * nDegreeToRadian; // nDegreeの値をラジアンに変換した値をnRadianに入れる

        //XY平面上を回転させる
        transform.position
            = new Vector3(Point.gameObject.transform.position.x + Mathf.Cos(nRadian) * nRadiusX,
            Point.gameObject.transform.position.y + Mathf.Sin(nRadian) * nRadiusZ,
            Point.gameObject.transform.position.z);

        if (player == null)
            return;
        
    }

    public GameObject GetPoint()
    {
        return Point;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && GetComponent<ReBound>() == null)
        {
            player = other.gameObject;
        }
    }
}
