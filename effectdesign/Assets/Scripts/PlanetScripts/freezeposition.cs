﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class freezeposition : MonoBehaviour
{
    public float y = 0.025f;
    
    // Start is called before the first frame update
    void Start()
    {
        transform.position = transform.parent.position+new Vector3(0,y,0);
    }

    // Update is called once per frame
    void Update()
    {
    }
}
