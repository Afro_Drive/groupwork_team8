﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 画像の回転打ち消し
/// </summary>
public class freezeRotate : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.identity;
    }
}
