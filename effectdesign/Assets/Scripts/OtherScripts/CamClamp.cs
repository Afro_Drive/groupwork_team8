﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// オブジェクトがこのコライダーに接触した面
/// </summary>
enum Surface
{
    Top, Buttom, Right, Left, None,
}

/// <summary>
/// コースアウトオブジェクトを基準としたカメラ移動の制御
/// 作成者:谷 永吾
/// </summary>
public class CamClamp : MonoBehaviour
{
    //フィールド
    BoxCollider myCollider; //このコライダー
    Surface hitSurface; //接触面

    // Start is called before the first frame update
    void Start()
    {
        //フィールドを初期化
        //自身のコライダーを取得
        myCollider = this.GetComponent<BoxCollider>();
        hitSurface = Surface.None;
    }

    // Update is called once per frame
    void Update()
    {

    }

    /// <summary>
    /// コライダーにオブジェクトが侵入してきた際の処理
    /// </summary>
    /// <param name="other">接触対象</param>
    void OnTriggerStay(Collider other)
    {
        //ここの処理が通らない(Playerは衝突するが、Cameraのコライダーが当たらない！)
        //Cameraタグを所有したオブジェクトのコライダーの場合のみ処理
        if (other.CompareTag("MainCamera"))
        {
            //接触対象と自身の座標を取得
            #region 接触対象
            //接触対象の左端
            Vector3 hitColPosL =
                other.transform.TransformPoint(
                    ((BoxCollider)other).center.x - ((BoxCollider)other).size.x / 2,
                    other.transform.position.y,
                    other.transform.position.z);
            //Debug.Log(hitColPosL.ToString());
            //Debug.Log(other.transform.position.ToString());
            //接触対象の右端
            Vector3 hitColPosR =
                other.transform.TransformPoint(
                  ((BoxCollider)other).center.x + ((BoxCollider)other).size.x / 2,
                  other.transform.position.y,
                  other.transform.position.z);
            //接触対象の上辺
            Vector3 hitColPosT =
                other.transform.TransformPoint(
                  other.transform.position.x,
                  ((BoxCollider)other).center.y + ((BoxCollider)other).size.y / 2,
                  other.transform.position.z);
            //接触対象の底辺
            Vector3 hitColPosB =
                other.transform.TransformPoint(
                  other.transform.position.x,
                  ((BoxCollider)other).center.y - ((BoxCollider)other).size.y / 2,
                  other.transform.position.z);
            #endregion 接触対象

            #region 自身の座標
            //自身の左端
            Vector3 myColPosL =
                myCollider.transform.TransformPoint(
                    myCollider.center.x - myCollider.size.x,
                    myCollider.transform.position.y,
                    myCollider.transform.position.z
                    );
            //自身の右端
            Vector3 myColPosR =
                myCollider.transform.TransformPoint(
                    myCollider.center.x + myCollider.size.x,
                    myCollider.transform.position.y,
                    myCollider.transform.position.z);
            //自身の上辺
            Vector3 myColPosT =
                myCollider.transform.TransformPoint(
                    myCollider.transform.position.x,
                    myCollider.center.y + myCollider.size.y / 2,
                    myCollider.transform.position.z);
            //自身の底辺
            Vector3 myColPosB =
                myCollider.transform.TransformPoint(
                    myCollider.transform.position.x,
                    myCollider.center.y - myCollider.size.y / 2,
                    myCollider.transform.position.z);
            #endregion 自身の座標

            //自身のタグに応じて判定箇所を選定し、座標を補正
            if (this.CompareTag("ReflectWallX"))
            {
                //接触対象との位置関係で場合分け
                if (hitColPosL.x <= myColPosR.x)
                {
                    hitColPosL.x = myColPosR.x;
                    hitSurface = Surface.Right;
                }
                else if(hitColPosR.x >= myColPosL.x)
                {
                    hitColPosR.x = myColPosL.x;
                    hitSurface = Surface.Left;
                }

            }
            if (this.CompareTag("ReflectWallY"))
            {
                if (hitColPosT.y >= myColPosB.y)
                {
                    hitColPosT.y = myColPosB.y;
                    hitSurface = Surface.Top;
                }
                else if(hitColPosB.y <= myColPosT.y)
                {
                    hitColPosB.y = myColPosT.y;
                    hitSurface = Surface.Buttom;
                }
            }
        }
    }
}
