﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 跳ね返り処理
/// 作成者:
/// </summary>
public class Reflection : MonoBehaviour
{
    bool timeswitch=false;
    float time = 30f;
    Collider other;
    void Start()
    {
        timeswitch = false;
        time = 30f;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            this.other = other;
            timeswitch = true;
        }
        
    }
    void Update()
    {
        if(timeswitch)
        {
            time = time - 1;
            other.transform.position+=new Vector3(-2f, 0, 0);
        }
        if(time==0)
        {
            time = 30f;
            timeswitch = false;
        }
    }
}
