﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.AI; 

public class PlayerChase : MonoBehaviour
{
    private GameObject target;

    void Update()
    {
        if (target == null)
            return;

        transform.position += (target.transform.position - transform.position) / 100;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            target = other.gameObject;
    }
}
