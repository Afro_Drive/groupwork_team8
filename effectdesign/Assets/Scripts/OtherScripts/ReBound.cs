﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Playerが当たるとはじき返す
/// </summary>
public class ReBound : MonoBehaviour
{
    private bool reBound;
    private GameObject player;
    private int timer;
    private Vector3 vec;
    private PlanetRevolution rev;

    // Start is called before the first frame update
    void Start()
    {
        timer = 120;
        rev = GetComponent<PlanetRevolution>();
        vec = Vector3.zero;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (!reBound)
        //    return;

        //if (timer == 0)
        //{
        //    reBound = false;
        //    return;
        //}

        //if (player.GetComponent<Rotation>().IsCatch)
        //{ player.GetComponent<Rotation>().Explode(); }

        //timer -= 1;
        //player.transform.position += vec;
        //vec /= 1.2f;

        //Debug.Log(vec);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            reBound = true;
            player = other.gameObject;
            vec = (player.transform.position - rev.GetPoint().transform.position).normalized;
            vec.z = 0;
            player.gameObject.GetComponent<Rotation>().Explode();
            player.transform.parent = null;
            player.gameObject.GetComponent<PlayerMover>().SetMoveVelocity(vec);
        //    player.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, 0);
        //    timer = 120;
        //    player.transform.parent = null;
        }
    }
}
