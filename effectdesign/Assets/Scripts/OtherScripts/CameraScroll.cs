﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// フォーカス対象の列挙型
/// 作成者:谷 永吾
/// </summary>
enum FocusModeIs
{
    Player,//Playerを捉えている
    ToPlayer,//Playerへフレーム移動中
    ToPlanet,//惑星へフレーム移動中
    Planet,//惑星を捉えている
}

/// <summary>
/// Playerの離脱速度に合わせたカメラワークの速度補正値
/// </summary>
enum LinerSpeed
{
    Low,//低い
    Middle,//普通
    High,//高い
}

/// <summary>
/// Sceneビューにおけるカメラ移動処理
/// 川村先生のUnity講座の資料を参照
/// 作成者:谷 永吾
/// </summary>
public class CameraScroll : MonoBehaviour
{
    //フィールド
    [SerializeField, Header("ゲーム開始時スクロールの基準となるオブジェクト")]
    public Transform initFocusTarget;
    [Header("カメラワークの速度")]
    public float speed = 5f;
    [Header("カメラ操作時のY方向の移動範囲")]
    public float moveRangeY = 7f;
    [Header("カメラ操作時のX方向の移動範囲")]
    public float moveRangeX = 7f;

    private Vector3 offset; //Playerと並走する際の座標補正値
    private BoxCollider focusCol; //カメラの動作範囲判定用コライダー

    //1フレーム前と現在フレームの比較用
    private FocusModeIs mode;//フォーカス対象
    private FocusModeIs prevMode; //こっちに切り替えよう。
    private Transform dynamicFocusTarget; //ゲーム中に動的に中身が変化するスクロール基準
    private Transform prevFocusTarget; //1フレーム前のスクロール対象

    #region 線形補間用の変数
    //ゲーム開始時のカメラの位置
    float initCamPosZ = 0f;
    //補間開始時のカメラの位置
    Vector3 currentCamPos = Vector3.zero;
    //補間移動予定の座標
    Vector3 endPos = Vector3.zero;
    //始点から終点までの距離
    float moveLeng = 0f;
    //補間移動開始時の時間記録変数
    float startTime = 0f;
    //惑星からPlayerへフォーカス移動する際の速度
    float speedToPlayer = 0f;
    //Playerへの補間割合用の変数
    float linerRateToP = 0.51f;
    //Playerの移動速度に合わせたカメラ移動の補正値を格納したディクショナリ
    Dictionary<LinerSpeed, float> linerRevisNum;
    #endregion 線形補間用の変数

    #region 不要になった変数群
    //private bool focusPlayer;//一部フォーカス処理の切り替えフラグ(これが複数モードを管理する場合はenumで管理する)
    //private bool prevPlayerState;
    //private bool currentPlayerState;
    #endregion 不要になった変数群

    // Start is called before the first frame update
    void Start()
    {
        //自身とスクロール対象のワールド座標との差分を補正値とする
        offset = this.transform.position - initFocusTarget.transform.position;

        //Playerをフォーカス中として初期化
        mode = FocusModeIs.Player;
        prevMode = FocusModeIs.Player;
        #region Playerのフラグの初期化→enum変数に委託
        //prevPlayerState = false;
        //currentPlayerState = false;
        #endregion Playerのフラグの初期化→enum変数に委託

        prevFocusTarget = null;

        //カメラ移動のコライダーをカラで初期化
        focusCol = null;

        //カメラの初期Zの位置を取得
        initCamPosZ = this.transform.position.z;

        //ディクショナリを初期化
        linerRevisNum = new Dictionary<LinerSpeed, float>()
        {
            { LinerSpeed.Low,    100f },
            { LinerSpeed.Middle, 200f },
            { LinerSpeed.High,   500f },
        };

    }

    // Update is called once per frame
    void Update()
    {
        //スクロール基準のオブジェクトが衛星軌道上にいない
        //(宇宙空間を浮遊している)
        if (!initFocusTarget.GetComponent<Rotation>().IsCatch)
        {
            //補間割合に合わせてフォーカスモードの更新
            if (linerRateToP > 0.5)
                mode = FocusModeIs.Player;
            else
                mode = FocusModeIs.ToPlayer;

            //惑星からPlayerへ線形的に移動中の処理
            //Playerが離脱した瞬間に補間の始点・終点を設定
            if ((prevMode == FocusModeIs.Planet ||
                 prevMode == FocusModeIs.ToPlanet) &&
                mode == FocusModeIs.ToPlayer)
            {
                //線形移動の始点・終点を設定
                SetPosFromPln2Ply();
                #region メソッドかにより削除
                ////カメラの現在位置を取得
                //currentCamPos = this.transform.position;
                ////カメラとPlayerの差分で2次元的ベクトルを取得
                //var distance = initFocusTarget.transform.position - currentCamPos;

                ////Playerの座標より少し先の移動位置を設定
                //var extention =
                //    new Vector3(distance.x, distance.y, 0).normalized * 5f;
                ////*5fの演算に影響されないよう、Z座標をここで設定
                //extention.z = initCamPosZ;
                //endPos = initFocusTarget.transform.position + extention;
                //Debug.Log(endPos.ToString());
                #endregion メソッドかにより削除
            }

            //惑星からPlayerへ線形補間移動
            LinerFocusOnPlayer(currentCamPos, endPos);
            //↑この処理がある程度進むとフォーカスモードが切り替わる

            //惑星からPlayerへカメラワークが移動し終わった
            if (mode == FocusModeIs.Player)
            {
                //Playerに追随する
                FocusOnPlayer();
                #region メソッド化により削除
                ////スクロールの基準を外部から設定されたオブジェクトに戻す
                //dynamicFocusTarget = initFocusTarget;

                ////毎フレーム、スクロール対象と一定の距離を保った状態にする
                ////これで対象が動くごとにCameraも動く
                //this.transform.position = dynamicFocusTarget.transform.position + offset;
                #endregion メソッド化により削除
            }
        }
        //衛星軌道上に合流している
        //または、Playerの親がフォーカス対象と異なる場合
        //(ただし、Nullの場合は浮遊中のため除外)
        else if (initFocusTarget.GetComponent<Rotation>().IsCatch ||
                (GameObject.FindWithTag("Player").transform.parent != null &&
                dynamicFocusTarget !=
                       GameObject.FindWithTag("Player").transform.parent))
        {
            #region 状態管理をenumに委託
            //現在フレームに捕縛されていると設定
            //currentPlayerState = true;
            //フォーカスモード切替をOFF(なんのフォーカスモードか分かりにくいな・・・)
            //focusPlayer = false;
            #endregion 状態管理をenumに委託
            //Playerへカメラを線形移動させた割合を初期化
            linerRateToP = 0;

            //オブジェクトを衛星としている惑星の情報を取得
            //これをスクロール基準に切り替える
            SetTargetToPlanet();
            #region メソッド化により削除
            ////Playerのタグを所有するオブジェクトを取得
            //dynamicFocusTarget = GameObject.FindWithTag("Player").transform;
            //if (dynamicFocusTarget.
            //    transform.
            //    parent.
            //    name.
            //    Contains("Rotation")) //親がRotationJoint(回転処理の透明ベアリング)の場合
            //{
            //    //RotationJointは惑星の子供のため、
            //    //Playerから見て、惑星は「親の親」となる
            //    dynamicFocusTarget =
            //        initFocusTarget.transform.parent.parent;
            //}
            //else //親が惑星本体
            //{
            //    //Playerの親を基準に
            //    dynamicFocusTarget =
            //        initFocusTarget.transform.parent;
            //}
            #endregion メソッド化により削除

            //その惑星が画面の中心に来るようにカメラ座標を調整
            //惑星のスプライトを所有するオブジェクトを取得
            var sprite = FindSpriteComp();
            #region メソッド化により削除
            ////惑星が所有する子供オブジェクトを取得する(この行で例外処理発生中)←解決済み
            //var targetChilds = GameObject.Find("/" + dynamicFocusTarget.name);
            //Transform sprite = null; //後ほど生成位置を吟味
            ////子供オブジェクトの中で画像(スプライト)を取得する
            //foreach (Transform child in targetChilds.transform)
            //{
            //    //SpriteRendererコンポーネントを持っていなければ飛ばす
            //    if (child.GetComponent<SpriteRenderer>() == null)
            //        continue;

            //    if (child.
            //        GetComponent<SpriteRenderer>().
            //        sprite.
            //        name == "木の画像")
            //    {
            //        //今後はより汎用的な名前にする(spriteとか)
            //        sprite = child; //変数に格納
            //    }
            //}
            #endregion メソッド化により削除

            //スプライトが取得できていれば
            if (sprite != null)
            {
                //そのスプライトに合わせたカメラのZ座標を算出
                float camPosZ = GetSpriteSize(sprite);
                #region メソッド化により削除
                ////スプライトの描画コンポーネント情報を取得
                //var rendererOfSprite =
                //    sprite.GetComponent<SpriteRenderer>();
                ////スプライトの大きさ(Vector3)を取得
                //var spriteSize =
                //    -rendererOfSprite.bounds.size;

                ////惑星の大きさに合わせたカメラのZ座標を算出
                //float camPosZ = spriteSize.x;
                #endregion メソッド化により削除

                //ここでカメラの移動予定位置まで線形的に移動する
                //前フレームでPlayerが捕縛されていない、
                //かつ
                //現在フレームでPlayerが捕縛されている時
                //または、前フレームと現在フレームでフォーカス対象が異なる時
                //線形補間用の変数を設定
                if ((mode == FocusModeIs.Player &&
                    prevMode == FocusModeIs.ToPlanet) ||
                    dynamicFocusTarget != prevFocusTarget)
                {
                    //線形移動の始点・終点を設定
                    SetPosFromPly2Pln(camPosZ);
                    #region メソッド化により削除
                    ////Playerが惑星の衛星軌道に合流した際の座標(始点)
                    //currentCamPos = this.transform.position;

                    ////カメラの移動予定座標(終点)
                    //endPos =
                    //    new Vector3(
                    //        dynamicFocusTarget.transform.position.x,
                    //        dynamicFocusTarget.transform.position.y,
                    //        camPosZ
                    //            );
                    #endregion メソッド化により削除
                    #region メソッド化により削除
                    ////始点から終点までの距離を算出
                    //moveLeng = Vector3.Distance(endPos, currentCamPos);

                    ////開始時間を取得
                    //startTime = Time.time;
                    #endregion メソッド化により削除
                }

                //カメラを線形的に移動させる
                LinerFocusOnPlanet(currentCamPos, endPos);
                #region メソッド化により削除
                ////目的地までのカメラの移動割合を算出
                //float rate =
                //    (Time.time - startTime) * speed / moveLeng;//(現在の移動量)/総移動量
                ////カメラを線形的に座標移動
                //this.transform.position =
                //    Vector3.Lerp(currentCamPos, endPos, rate);
                #endregion メソッド化により削除

                //移動しきったら、惑星を完全に捉える
                if (mode == FocusModeIs.Planet)
                    FocusOnPlanet(endPos);
                #region 座標の移動を線形的なものに変更
                //惑星から特定の位置にカメラを移動させる
                //(惑星とカメラのX,Y座標を同一にする事で正面に来る)
                //this.transform.position =
                //    new Vector3(
                //    dynamicFocusTarget.transform.position.x,
                //    dynamicFocusTarget.transform.position.y,
                //    camPosZ
                //        );
                #endregion 座標の移動を線形的なものに変更
            }
        }

        //カメラ移動のキー入力受付(惑星へフォーカス中のみ)
        if (mode == FocusModeIs.Planet)
            MoveXY();

        //フォーカス記録を更新
        prevMode = mode;
        //現在フレームのフォーカス対象を前フレームに代入する
        prevFocusTarget = dynamicFocusTarget;
    }

    /// <summary>
    /// Playerに追随するカメラワーク
    /// </summary>
    private void FocusOnPlayer()
    {
        //スクロールの基準を外部から設定されたオブジェクトに戻す
        dynamicFocusTarget = initFocusTarget;

        //毎フレーム、スクロール対象と一定の距離を保った状態にする
        //これで対象が動くごとにCameraも動く
        this.transform.position =
            dynamicFocusTarget.transform.position + offset;

        //1フレーム前のフォーカス対象を空にする
        prevFocusTarget = null;
    }

    /// <summary>
    /// Playerが公転中、撮影対象を
    /// Playerが公転している惑星に切り替える
    /// </summary>
    private void SetTargetToPlanet()
    {
        //Playerの親を検索し、スクロール対象とする
        //Playerのタグを所有するオブジェクトを検索・取得
        dynamicFocusTarget = GameObject.FindWithTag("Player").transform;
        if (dynamicFocusTarget == null)
            Debug.Log("nullだよ！");
        if (dynamicFocusTarget.
            transform.
            parent.
            name.
            Contains("Rotation")) //親がRotationJoint(回転処理の透明ベアリング)の場合
        {
            //RotationJointは惑星の子供のため、
            //Playerから見て、惑星は「親の親」となる
            dynamicFocusTarget =
                initFocusTarget.transform.parent.parent;

        }
        else //親が惑星本体
        {
            //Playerの親を基準に
            dynamicFocusTarget =
                initFocusTarget.transform.parent;
        }
        //Debug.Log(dynamicFocusTarget.ToString());
    }

    /// <summary>
    /// 惑星のスプライトのコンポーネントを検索・取得
    /// </summary>
    /// <returns>惑星にアタッチされたSpriteRendererのspriteコンポーネント</returns>
    private Transform FindSpriteComp()
    {
        //惑星が所有する子供オブジェクトを取得する(この行で例外処理発生中)←解決済み
        var targetChilds = GameObject.Find("/" + dynamicFocusTarget.name);

        Transform sprite = null;
        //子供オブジェクトの中で画像(スプライト)を取得する
        foreach (Transform child in targetChilds.transform)
        {
            //SpriteRendererコンポーネントを持っていなければ飛ばす
            if (child.GetComponent<SpriteRenderer>() == null)
                continue;

            if (child.
                GetComponent<SpriteRenderer>().
                CompareTag("PlanetSprite")) 
            {
                sprite = child; //変数に格納
            }
        }
        //画像データを返却
        return sprite;
    }

    /// <summary>
    /// 惑星にアタッチされたスプライトの大きさ取得
    /// </summary>
    /// <param name="sprite">大きさを調べたいスプライトがアタッチされたオブジェクトのトランスフォーム</param>
    /// <returns>スプライトの大きさ</returns>
    private float GetSpriteSize(Transform sprite)
    {
        //スプライトの描画コンポーネント情報を取得
        var rendererOfSprite =
            sprite.GetComponent<SpriteRenderer>();
        //スプライトの大きさ(Vector3)を取得
        var spriteSize =
            -rendererOfSprite.bounds.size;

        //惑星の大きさを返却
        return spriteSize.x;
    }

    /// <summary>
    /// Playerから惑星へカメラを線形的に移動
    /// (Playerが惑星の衛星軌道に突入した際)
    /// </summary>
    /// <param name="startPos">惑星の衛星軌道に突入時のカメラの座標</param>
    /// <param name="endPos">カメラを移動させたい座標</param>
    private void LinerFocusOnPlanet(Vector3 startPos, Vector3 endPos)
    {
        //カメラの移動情報が登録されていなければ何もしない
        if (Vector3.Distance(endPos, currentCamPos) == 0)
            return;

        //前フレームでPlayerが捕縛されていない、かつ
        //現在フレームでPlayerが捕縛されている時
        //または、
        //現在と前フレームでフォーカス対象が異なる場合に
        if ((mode == FocusModeIs.Player && prevMode == FocusModeIs.ToPlanet)
            || dynamicFocusTarget != prevFocusTarget)
        {
            //始点から終点までの距離を算出
            moveLeng = Vector3.Distance(endPos, currentCamPos);

            //開始時間を取得
            startTime = Time.time;
        }

        //目的地までのカメラの移動割合を算出
        float rate =
            (Time.time - startTime) * speed / moveLeng;//(現在の移動量)/総移動量
        //カメラを線形的に座標移動
        this.transform.position =
            Vector3.Lerp(currentCamPos, endPos, rate);

        //完全に惑星に移動しきったら、
        if (rate >= 1)
        {
            //惑星にフォーカスモードにする
            //FocusOnPlanet(endPos);
            mode = FocusModeIs.Planet;
        }
        else//まだなら、
        {
            //惑星へ移動中とする
            mode = FocusModeIs.ToPlanet;
        }
    }

    /// <summary>
    /// 惑星からPlayerへカメラを線形的に移動
    /// </summary>
    private void LinerFocusOnPlayer(Vector3 startPos, Vector3 endPos)
    {
        //引数に値が登録されていなければ終了
        if (Vector3.Distance(endPos, startPos) == 0)
            return;

        //フォーカス対象をPlayerへ変更
        dynamicFocusTarget = initFocusTarget;

        //Playerが、前フレームで捕縛状態、かつ
        //現在フレームでは離脱状態の時のみ
        if ((prevMode == FocusModeIs.Planet ||
            prevMode == FocusModeIs.ToPlanet) &&
            mode == FocusModeIs.ToPlayer)
        {
            //始点から終点までの距離を算出
            moveLeng = Vector3.Distance(endPos, startPos);

            //開始時間を取得
            startTime = Time.time;

            //Playerの離脱速度に応じた補間速度を設定
            speedToPlayer =
                initFocusTarget.
                GetComponent<PlayerMover>().
                Velocity.magnitude;

            //Playerの移動速度に応じた補正値を選択
            var revision = LinerSpeed.High;
            if (speedToPlayer > 0.01f && speedToPlayer < 0.15f)
                revision = LinerSpeed.Middle;
            else if (speedToPlayer > 0.15f)
                revision = LinerSpeed.Low;

            //Playerの離脱速度を補正
            speedToPlayer =
                speedToPlayer * linerRevisNum[revision];

            //Debug.Log(speedToPlayer.ToString());
            //Debug.Log(initFocusTarget.
            //      GetComponent<PlayerMover>().
            //      Velocity.magnitude.ToString());
        }

        //目的地までのカメラの移動割合を算出
        //(モード管理用にフィールドの変数に代入)
        linerRateToP =
            (Time.time - startTime) * speedToPlayer / moveLeng;//(現在の移動量)/総移動量
        //カメラを線形的に座標移動
        this.transform.position =
            Vector3.Lerp(startPos, endPos, linerRateToP);

        #region Debug用処理
        //Debug.Log(linerRateToP.ToString());
        //Debug.Log(startPos.ToString());
        //Debug.Log(endPos.ToString());
        //Debug.Log(speedToPlayer.ToString());
        //Debug.Log("ToPlayer");
        #endregion Debug用処理

        //ある程度補間が進んだら、
        if (linerRateToP >= 0.5)
        {
            //フォーカスモードを切り替え、Playerへ追随する処理に切り替え
            //focusPlayer = true;
            mode = FocusModeIs.Player;
        }
        else//まだなら
        {
            //Playerへ移動中にする
            mode = FocusModeIs.ToPlayer;
        }
    }

    /// <summary>
    /// 惑星を真ん中に映す
    /// </summary>
    /// <param name="setPos">カメラが定着する位置</param>
    private void FocusOnPlanet(Vector3 setPos)
    {
        //自身の座標を引数にする
        this.transform.position = setPos;
    }

    /// <summary>
    /// 惑星からPlayerへフォーカスする際の
    /// 始点・終点の演算・設定
    /// </summary>
    private void SetPosFromPln2Ply()
    {
        //カメラの現在位置を取得
        currentCamPos = this.transform.position;
        //カメラとPlayerの差分で2次元的ベクトルを取得
        var distance = initFocusTarget.transform.position - currentCamPos;

        //Playerの座標より少し先の移動位置を設定
        var extention =
            new Vector3(distance.x, distance.y, 0).normalized * 5f;
        //*5fの演算に影響されないよう、Z座標をここで設定
        extention.z = initCamPosZ;
        endPos = initFocusTarget.transform.position + extention;
    }

    /// <summary>
    /// Playerから惑星へフォーカスする際の
    /// 始点・終点の演算・設定
    /// </summary>
    /// <param name="camPosZ">カメラのZ軸の到着予定位置</param>
    private void SetPosFromPly2Pln(float camPosZ)
    {
        //Playerが惑星の衛星軌道に合流した際の座標(始点)
        currentCamPos = this.transform.position;

        //カメラの移動予定座標(終点)
        endPos =
            new Vector3(
                dynamicFocusTarget.transform.position.x,
                dynamicFocusTarget.transform.position.y,
                camPosZ
                    );
    }

    /// <summary>
    /// カメラをキー操作で移動させる
    /// </summary>
    private void MoveXY()
    {
        //アローキーが入力されたら方向に合わせて指定された範囲だけ移動

        this.transform.position =
            new Vector3(
                this.transform.position.x
                + Input.GetAxis("HorizontalR") * moveRangeX,
                this.transform.position.y
                + Input.GetAxis("VerticalR") * moveRangeY,
                this.transform.position.z);


        //線形補間移動が必要そうだ。

        #region 【削除】十字キー入力の場合分け
        //if (Input.GetKey(KeyCode.UpArrow))
        //{
        //}
        //else if (Input.GetKey(KeyCode.DownArrow))
        //{
        //this.transform.position =
        //    new Vector3(
        //        this.transform.position.x,
        //        this.transform.position.y
        //        + Input.GetAxis("Horizontal") * moveSpeed,
        //        this.transform.position.z);
        //}
        //else if (Input.GetKey(KeyCode.RightArrow))
        //{
        //this.transform.position +=
        //    new Vector3(moveSpeed, 0, 0);
        //}
        //else if (Input.GetKey(KeyCode.LeftArrow))
        //{
        //this.transform.position +=
        //    new Vector3(-moveSpeed, 0, 0);
        //}
        #endregion 【削除】十字キー入力の場合分け
        //Debug.Log(this.transform.position.ToString());
    }

    private void ClampMove()
    {
        //コライダー内にコースアウト用オブジェクトが侵入した時
        if (focusCol.CompareTag("ReflectWall"))
        {

        }
    }

    /// <summary>
    /// 現在のフォーカス対象の取得
    /// </summary>
    /// <returns>フィールドmode</returns>
    private FocusModeIs GetFocusMode()
    {
        return mode;
    }
}


