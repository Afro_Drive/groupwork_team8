﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 特定惑星に接近する小惑星
/// 作成者:
/// </summary>
public class Impactplanet : MonoBehaviour
{
    public GameObject parentPlanet;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Destroy(gameObject);
            other.GetComponent<Boost1>().AddDust(100);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
}
