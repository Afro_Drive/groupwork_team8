﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// UnityEngineが用意するTimeクラスの使い方の練習
/// 参考サイト: https://gametukurikata.com/program/time
/// 作成者:谷 永吾
/// </summary>
public class TimerTest : MonoBehaviour
{
    //フィールド
    //分
    private int minutes;
    //秒
    private float seconds;
    //1フレーム前のUpdate時の秒数
    private float previousSeconds;
    //制限時間
    private float limit;

    // Start is called before the first frame update
    void Start()
    {
        //各種変数を初期化
        minutes = 0;
        seconds = 0f;
        previousSeconds = 0f;
        limit = 60f;
    }

    // Update is called once per frame
    void Update()
    {
        //CountUPTimer
        //Unity内の最終フレーム更新にかかった時間を加算
        seconds += Time.deltaTime;
        if (seconds >= 60f)//累計加算秒数が60に達したら
        {
            //分に加算
            minutes++;
            //秒を戻す(小数点以下はそのまま残す)
            seconds = seconds - 60f;
        }
        //1フレーム前の秒数に現在フレームの秒数を格納
        previousSeconds = seconds;

        //CountDownTimer
        if (limit >= 0)
        {
            limit -= Time.deltaTime;
            Debug.Log(limit);
        }
        else
            Debug.Log("TimeUP!!");

    }
}
