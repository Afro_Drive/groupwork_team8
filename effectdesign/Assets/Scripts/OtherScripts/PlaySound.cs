﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySound : MonoBehaviour
{
    //音声ファイル格納用変数
    public AudioClip sound01;
    public AudioClip sound02;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
    }
    void Update()
    {

        //指定のキーが押されたら音声ファイルの再生をする
        if (Input.GetKeyDown(KeyCode.K))
        {
            audioSource.clip = sound01;
            audioSource.Play();
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            audioSource.clip = sound02;
            audioSource.Play();
        }
    }
}
