﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Playerの高速移動で破壊可能な小惑星
/// (破壊後はチリを一定量生成)
/// 作成者:
/// </summary>
public class smallPlanet : MonoBehaviour
{
    private float Bounse;
    private bool ReBound;
    private Collider Player;

    void Start()
    {
        ReBound = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (ReBound)
        {
            Player.transform.position += (Player.transform.position - transform.position) * Bounse / 10;
            Bounse /= 1.2f;
            if (Mathf.Abs(Vector3.Distance(transform.position, Player.transform.position)) >= 15) 
            {
                ReBound = false;
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag != "Player")
        { return; }
        Player = collision;
        if (Player.gameObject.GetComponent<PlayerMover>().GetAccel() <= 3)
        {
            Bounse = 3.0f;
            ReBound = true;
        }
        else
        {
            for (int i = 0; i < 5; i++)
            {
                GameObject obj = (GameObject)Resources.Load("Dust");
                Instantiate(obj, new Vector3(transform.position.x - Random.Range(-5, 5), transform.position.y, transform.position.y - Random.Range(-5, 5)), Quaternion.identity);
            }
            Destroy(gameObject);
        }
    }
}
