﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 取得可能なチリ
/// 作成者:
/// </summary>
public class dust : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject parent = gameObject.transform.parent.gameObject;
            Destroy(parent);
            Destroy(gameObject);
            other.GetComponent<Boost1>().AddDust(100);
        }
    }
}
