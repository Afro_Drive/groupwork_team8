﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 所有しているチリの個数を表すUI
/// 作成者:
/// </summary>
public class DustEnergyUI : MonoBehaviour
{
    Text text;
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        text = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        text.text = Player.GetComponent<Boost1>().DustCount().ToString();
    }
}
