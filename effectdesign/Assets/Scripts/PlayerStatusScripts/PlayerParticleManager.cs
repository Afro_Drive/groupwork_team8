﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerParticleManager : MonoBehaviour
{
    private bool play;
    [SerializeField, Header("ブースト時のパーティクル")] ParticleSystem boostParticle;
    [SerializeField, Header("公転離脱時のパーティクル")] ParticleSystem breakawayParticle;
    
    private Vector3 direction;
    private bool boost;

    private void Start()
    {
        play = false;

    }
    // Update is called once per frame
    void Update()
    {
        if (!play)
            return;

        if (boost)
        {
            boostParticle.transform.position -= (direction * 0.01f);
            return;
        }
        else
        {
            breakawayParticle.transform.position -= (direction * 0.01f);
        }
    }

    public void BoostParticleStart(Vector3 direction)
    {
        boostParticle.transform.position = transform.position;
        this.direction = direction.normalized;
        boostParticle.transform.rotation = Quaternion.LookRotation(new Vector3(direction.y, -direction.x, 0), Vector3.forward);
        boostParticle.Play();
        play = true;
        boost = true;
    }
    public void BoostParticleEnd()
    {
        boostParticle.Stop();
        play = false;
        boostParticle.transform.rotation = Quaternion.identity;
    }

    public void BreakawayParticleStart(Vector3 direction)
    {
        breakawayParticle.transform.position = transform.position;
        this.direction = direction.normalized;
        breakawayParticle.transform.rotation = Quaternion.LookRotation(new Vector3(direction.y, -direction.x, 0), Vector3.forward);
        breakawayParticle.Play();
        play = true;
        boost = true;
    }
}
