﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeTest : MonoBehaviour
{
    float angle;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        angle += 0.1f;

        
        transform.localPosition =  new Vector3(Mathf.Cos(angle) * 5, 0, Mathf.Sin(angle) * 5);
    }
}
