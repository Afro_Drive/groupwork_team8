﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// チリを大量消費するPlayerの高速移動
/// 作成者:鍬さん
/// </summary>
public class Boost1 : MonoBehaviour
{
    //フィールド
    //所有しているチリの量
    [SerializeField]
    int Dust;

    private PlayerMover mover;
    private Rotation rotation;
    private PlayerParticleManager manager;

    private bool isGameOver;

    private void Start()
    {
        //初期時のチリの所有量を設定
        //Dust = 10;
        isGameOver = false;
        mover = GetComponent<PlayerMover>();
        rotation = GetComponent<Rotation>();
        manager = GetComponent<PlayerParticleManager>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (Dust > 0 && rotation.IsCatch && (Input.GetButtonDown("Fire3") || Input.GetKeyDown(KeyCode.X)))
        {
            Dust -= 1;
            rotation.SpeedUp();
        }

        //記述位置を下のif節に移動
        //if (!mover.CanBoost())
        //return;

        if (Dust > 0 //チリを所有していて、
            && //かつ、
            (Input.GetKeyDown(KeyCode.Z)//ZキーまたはFire2ボタンが押されている
            || Input.GetButtonDown("Fire2"))
            && //カツ
            mover.CanBoost()//ブーストができる状態
                            //&& //かつ、
                            //(Input.GetAxis("Horizontal") != 0 //Playerの移動量が生じている
                            //|| Input.GetAxis("Vertical") != 0)) 
               )
        {
            //チリを１消費して、
            //Dust -= 1;
            //PlayerMoverのブースト処理を実行
            //mover.Boost();
        }

        if (Dust <= 0)
        {
            isGameOver = true;
        }
        //Debug.Log(Dust.ToString());
    }

    /// <summary>
    /// チリゲット
    /// </summary>
    /// <param name="dust">獲得チリ数</param>
    public void AddDust(int dust)
    {
        //自身の所有チリに加算する
        Dust += dust;
    }

    /// <summary>
    /// 現在のチリ所有量の取得
    /// </summary>
    /// <returns>フィールドのDustの値</returns>
    public int DustCount()
    { return Dust; }

    public bool IsGameOver()
    {
        return isGameOver;
    }
}

