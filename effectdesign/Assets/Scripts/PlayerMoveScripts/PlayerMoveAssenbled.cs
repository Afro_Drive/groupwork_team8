﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoveAssenbled : MonoBehaviour
{
    private bool inertia;　  //慣性のon/off
    private float y;　       //y方向の入力量
    private float x;　       //x方向の入力量
    private float accel;     //加速度
    private float normalAccel; //基本の加速度

    private float memAccel;  //加速度の記録用
    private bool isBoost;    //ブースト中か
    private bool boostEnd;   //ブーストの加速が終わったか
    private bool isVertical;//横向きになるかどうか
    [SerializeField, Header("ブーストの加速度")]
    float boostAccel;
    [SerializeField, Header("ブーストの加速度の最大値")]
    float boostMax;
    [SerializeField, Header("ブーストの減速度")]
    float boostDeceleration;

    [SerializeField, Header("加速度の初期値")]
    float accelInitial = 0.5f;
    [SerializeField, Header("加速度の増加具合")]
    float acceleration = 1.01f;

    private Vector3 velocity;       //入力方向
    private Vector3 beforVelocity;  //前フレームの入力方向
    private Vector3 moveVelocity;   //移動方向

    // Start is called before the first frame update
    bool Iscatch;//重力下にあるかどうか。
    bool timestart;
    bool IsSpace;
    bool Interval;
    float boost = 3;
    float time = 1f;
    float intervaltime = 60f;
    Rigidbody rigid;
    Collider hit;

    float nDegree = 0.0f; // 度数値
    float yDegree = 0.0f;
    public float nSpeed = 20.0f; // オブジェクトの移動するスピード
    float nDegreeToRadian = Mathf.PI / 180.0f; // nDegreeの値をラジアンに変換する
    float nRadian; // nDegreeの値をラジアンに変換した値
    float yRadian;
    public float nCenterX = 0.0f; // 楕円運動の水平中心座標
    public float nCenterZ = 0.0f; // 楕円運動の垂直中心座標
    public float nRadiusX = 10.0f; // 楕円運動の水平方向の半径
    public float nRadiusZ = 10.0f; // 楕円運動の垂直方向の半径
    public float speedgear = 1;
    public float radiuscontrol = 20f;
    
    float vx;
    float vz;
    float gx;
    float gz;
    float rotation;
    float angle;

    float test;
    /// <summary>
    /// ランダム関係
    /// </summary>
    [SerializeField, Header("移動量がない時のランダム移動量")]
    float randomMinX;
    [SerializeField]
    float randomMaxX;
    [SerializeField]
    float randomMinY;
    [SerializeField]
    float randomMaxY;
    [SerializeField]
    float randomSpeedMin;
    [SerializeField]
    float randomSpeedMax;

    private Vector3 randomVelocity;
    private Vector3 rMoveVelocity;
    private float rSpeed;

    private bool isMove;
    private int count;

    private Vector3 vectorCheck;
    private void Start()
    {
        inertia = false;
        isMove = false;
        accel = accelInitial;
        memAccel = 1;

        IsSpace = false;
        Iscatch = false;
        hit = null;
        time = 1;
        timestart = false;
        Interval = false;
        intervaltime = 60f;
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        //Debug.Log(isMove.ToString() + ", " + GetComponent<Rotation>().IsCatch());
        //もし公転中ならreturn
        if (IsCatch())
        {
            if (isMove)
                isMove = false;
            if (moveVelocity == Vector3.zero && accel == accelInitial)
                return;
            else
            {
                moveVelocity = Vector3.zero;
                accel = accelInitial;
                return;
            }
        }
        //Debug.Log(Interval);
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1"))
        {
            if (Iscatch)
            {
                transform.position = transform.position - new Vector3(0, 0, transform.position.z - transform.parent.position.z);

                IsSpace = true;
                Interval = true;
            }
        }
        if (IsSpace)
        {
            Iscatch = false;


            transform.parent = null;
            Initialize();//重力下から離れることが出来ます。離れることにより、惑星との親子関係を解消します。
            IsSpace = false;
        }
        if (Iscatch == false)
        {
            //transform.position += new Vector3(x, y, 0) / 5;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            speedgear = 0;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {
            speedgear = 1;
        }
        if (timestart)
        {
            time = time - 1;
        }
        if (time == 0)
        {
            Iscatch = true;
            // Set();
            nDegree = Mathf.Atan2((transform.position.y - hit.transform.position.y), (transform.position.x - hit.transform.position.x)) * 180.0f / Mathf.PI;
            timestart = false;
            time = 1;
        }//カウント終了後、ローテンション開始。開始前にプレイヤー自身と惑星の距離を計算。度数化。

        if (hit == null)
        {
            if (Iscatch)
            {
                Iscatch = false;
                Initialize();
            }
            return;
        }

        if (Iscatch)
        {
            /*timecount++;
            transform.RotateAround(hit.gameObject.transform.position, Vector3.up, angle * Time.deltaTime * boost);*/
            nDegree = nDegree + nSpeed; // 移動する分の値をnDegreeに入れる
            //nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
            nRadian = nDegree * nDegreeToRadian; // nDegreeの値をラジアンに変換した値をnRadianに入れる

            transform.localPosition = new Vector3(Mathf.Cos(nRadian) * 1, Mathf.Sin(nRadian) * 1, 0);
            //ここで回転。

            //transform.position += new Vector3(0, Mathf.Sin(yRadian)*nRadiusX,0);

        }
        //Debug.Log(Mathf.Cos(nRadian));
        //移動入力取得
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        //if(isMove==false)
        //{
        //    VectorCheck();
        //    if (vectorCheck.x > 0&&vectorCheck.y>=0)
        //    {
        //        if(isVertical)
        //        {
        //            x = -1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = 1;
        //        }
        //    }
        //    else if (vectorCheck.x <= 0 && vectorCheck.y >= 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = -1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = -1;
        //        }

        //    }
        //    else if (vectorCheck.x <= 0 && vectorCheck.y < 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = 1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = -1;
        //        }
        //    }
        //    else if (vectorCheck.x > 0 && vectorCheck.y >= 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = 1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = 1;
        //        }
        //    }
        //}
        velocity = new Vector3(x, y, 0).normalized;
        normalAccel = accel * memAccel;

        //コースアウト処理中でなく
        if (!isMove)
        {
            //もし移動入力があれば
            if (velocity != Vector3.zero)
            {
                //慣性のフラグをfalse
                if (inertia)
                    inertia = false;
                //入力方向が違ったら移動方向を変更
                if (moveVelocity.normalized != velocity)
                    moveVelocity = velocity;
                //移動
                transform.position += moveVelocity * normalAccel;
                //加速度増加
                if (accel <= 3)
                {
                    accel *= acceleration;
                }
            }

            //もし前フレームの入力した移動量より今の入力した移動量が小さかったら
            else if (velocity.magnitude < beforVelocity.magnitude && !isBoost && velocity.magnitude == 0)
            {
                //慣性のフラグteue
                inertia = true;
                //移動量に加速度をかけて
                moveVelocity *= accel;
                //加速度を初期化
                accel = accelInitial;
            }

            //ほぼ移動していないに等しかったら
            else if (moveVelocity.magnitude <= 0.01f)
            {
                //うごうご
                RandomMove();
            }
        }

        //慣性がtrueなら
        if (inertia)
        {
            if (isMove)
                transform.position += moveVelocity;
            else
                //最後に入力した方向に加速度を維持したまま移動
                transform.position += moveVelocity * memAccel;
        }

        if (isBoost)
        {
            if (!boostEnd)
            {
                if (accel < boostMax)
                    accel *= boostAccel;
                else
                    boostEnd = true;
            }
            else
            {
                if (accel > normalAccel)
                    accel /= boostDeceleration;
                else
                {
                    accel = accelInitial;
                    memAccel += 0.5f;
                    inertia = true;
                    isBoost = false;
                }
            }
        }

        //前フレームの入力方向を今の入力方向に
        beforVelocity = velocity;
    }
    void Update()
    {
        vectorCheck = transform.localPosition;
        Debug.Log(vectorCheck);
        if (Interval)
        {
            transform.parent = null;
            intervaltime = intervaltime - 1;
        }
        if (intervaltime == 0)
        {
            Interval = false;
            intervaltime = 60f;
        }
    }
    void VectorCheck()//横か縦か
    {
        float vc;
        vc = Mathf.Abs(vectorCheck.x) - Mathf.Abs(vectorCheck.y);
        if (vc >= 0)
        {
            isVertical = false;//縦向きに
        }
        else if (vc < 0)
        {
            isVertical = true;//横向きに
        }

    }
    void Initialize()
    {
        nRadian = 0;
        nDegree = 0;
        transform.rotation = Quaternion.identity;
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (isMove && other.tag == "Star")
    //    {
    //        moveVelocity = Vector3.zero;
    //    }
    //}
    void OnTriggerEnter(Collider hit)
    {
        if (hit.CompareTag("Star") && Interval == false)
        {
            this.hit = hit;
            timestart = true;

        }//惑星の重力圏に入った場合、タイムカウントが開始されます。カウントが終了したらローテンション開始。

    }

    /// <summary>
    /// うごうご
    /// </summary>
    private void RandomMove()
    {
        //移動量がなかったら
        if (randomVelocity == Vector3.zero)
        {
            //最大速度と移動量をランダムで設定
            randomVelocity = new Vector3(Random.Range(randomMinX, randomMaxX), Random.Range(randomMinY, randomMaxY), 0);
            rSpeed = Random.Range(randomSpeedMin, randomSpeedMax);
        }

        //移動
        rMoveVelocity += randomVelocity / 10;
        transform.position += rMoveVelocity;
        //移動速度が最大速度に達してたら初期化
        if (rMoveVelocity.magnitude >= rSpeed)
        {
            rMoveVelocity = Vector3.zero;
            randomVelocity = Vector3.zero;
        }
    }

    /// <summary>
    /// 加速度取得
    /// </summary>
    /// <returns></returns>
    public float GetAccel()
    {
        return accel * memAccel;
    }

    /// <summary>
    /// 加速度設定
    /// </summary>
    /// <param name="accel"></param>
    public void SetAccel(float accel)
    {
        this.accel = accel;
    }

    /// <summary>
    /// コースアウト
    /// </summary>
    public void CorseOut()
    {
        //公転から脱出してから帰還までが早いとRotationのIntervalと干渉してIscatchがtrueにならなかったので帰還速度は遅めにしてます。
        moveVelocity = (SearchPlanet().transform.position - transform.position) / 200;
        isMove = true;
        inertia = true;
    }

    /// <summary>
    /// 一番近い惑星検索
    /// コードお借りしました　http://beatdjam.hatenablog.com/entry/2014/10/22/032751
    /// </summary>
    /// <returns></returns>
    GameObject SearchPlanet()
    {
        float tmpDis = 0;           //距離用一時変数
        float nearDis = 0;          //最も近いオブジェクトの距離
                                    //string nearObjName = "";    //オブジェクト名称
        GameObject targetObj = null; //オブジェクト

        //タグ指定されたオブジェクトを配列で取得する
        foreach (GameObject obs in GameObject.FindGameObjectsWithTag("Star"))
        {
            //自身と取得したオブジェクトの距離を取得
            tmpDis = Vector3.Distance(obs.transform.position, transform.position);

            //オブジェクトの距離が近いか、距離0であればオブジェクト名を取得
            //一時変数に距離を格納
            if (nearDis == 0 || nearDis > tmpDis)
            {
                nearDis = tmpDis;
                //nearObjName = obs.name;
                targetObj = obs;
            }

        }
        //最も近かったオブジェクトを返す
        //return GameObject.Find(nearObjName);
        return targetObj;
    }

    public void Boost()
    {
        if (isBoost)
            return;

        isBoost = true;
        boostEnd = false;
        inertia = false;
    }
    public void Explode()
    {
        transform.position = transform.position - new Vector3(0, 0, transform.position.z - transform.parent.position.z);

        IsSpace = true;
        Interval = true;
    }

    /// <summary>
    /// 捕縛状態か？
    /// </summary>
    /// <returns>フィールドIsCatch</returns>
    public bool IsCatch()
    {
        return Iscatch;
    }
}
