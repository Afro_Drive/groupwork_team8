﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// ゲーム開始時の移動方向の列挙型
/// </summary>
enum InitDirection
{
    right, left, up, down,
}

/// <summary>
/// Playerの移動関係
/// </summary>
public class PlayerMover : MonoBehaviour
{
    //フィールド
    private float y;　       //y方向の入力量
    private float x;　       //x方向の入力量
    private float accel;     //加速度
    private float normalAccel; //基本の加速度

    private float memAccel;  //加速度の記録用
    private bool boostEnd;   //ブーストの加速が終わったか
    private bool isVertical;//横向きになるかどうか
    [SerializeField, Header("ブーストの加速度")]
    float boostAccel = 1.5f;
    [SerializeField, Header("ブーストの加速度の最大値")]
    float boostMax = 2.5f;
    [SerializeField, Header("ブーストの減速度")]
    float boostDeceleration = 1.5f;

    [SerializeField, Header("加速度の初期値")]
    float accelInitial = 1.5f;
    //[SerializeField, Header("加速度の増加具合")]
    //float acceleration = 1.01f;
    [SerializeField, Header("加速の上限値")]
    float accelMax = 2.0f;

    [SerializeField, Header("重力圏で引き寄せられる強さ")]
    float gravity = 0.25f;

    [SerializeField, Header("ゲーム開始時の移動方向")]
    InitDirection initMoveDir = InitDirection.right; //初期移動方向

    private Vector3 moveDir;   //移動方向
    private Vector3 getKeyDir;       //入力方向
    private Vector3 beforeDir;  //前フレームの入力方向

    private int count;
    private bool Interval;
    private Collider hit;

    /// <summary>
    /// ランダム関係
    /// </summary>
    //[SerializeField, Header("移動量がない時のランダム移動量")]
    //float randomMinX;
    //[SerializeField]
    //float randomMaxX;
    //[SerializeField]
    //float randomMinY;
    //[SerializeField]
    //float randomMaxY;
    //[SerializeField]
    //float randomSpeedMin;
    //[SerializeField]
    //float randomSpeedMax;

    private Vector3 randomVelocity;
    private Vector3 rMoveVelocity;
    private float beforBoostAccel;

    private Vector3 vectorCheck;
    private PlayerParticleManager manager;
    private Rotation rotation;
    private Boost1 boost;

    private bool coseoutMin;

    playerStatus status;

    private enum playerStatus
    {
        free,
        courseOut,
        rotate,
        boost,
    }

    private void Start()
    {
        Interval = false;
        coseoutMin = false;
        accel = accelInitial;
        memAccel = 1;
        count = 60;
        manager = GetComponent<PlayerParticleManager>();

        InGrav = false;
        hit = null;
        rotation = GetComponent<Rotation>();
        boost = GetComponent<Boost1>();

        //移動方向を初期化
        switch (initMoveDir)
        {
            case InitDirection.right:
                moveDir = Vector3.right;
                break;

            case InitDirection.left:
                moveDir = Vector3.left;
                break;

            case InitDirection.up:
                moveDir = Vector3.up;
                break;

            case InitDirection.down:
                moveDir = Vector3.down;
                break;
        }
        //プレイヤーの状態の初期化
        status = playerStatus.free;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //惑星の重力圏に突入した際の処理
        GravCatch();

        //公転中なら
        if (rotation.IsCatch)
        {
            //Playerがコースアウト状態なら、
            if (status == playerStatus.courseOut)
            {
                status = playerStatus.rotate; //コースアウト時の引き戻しを無効にする
                accel = accelInitial;
                coseoutMin = false;
            }
            //ブースト状態なら
            if (status == playerStatus.boost)
            {
                //ブースト状態を解除
                status = playerStatus.rotate;
                manager.BoostParticleEnd();
            }
            //移動方向が設定されてなければ終了
            //if (moveDir == Vector3.zero /*&& accel == accelInitial*/)
            //return;
            #region 公転軌道から離脱した後フリーズする原因のため削除
            //else
            //{
            ////移動方向を初期化
            //moveDir = Vector3.zero;
            ////accel = accelInitial;
            ////処理終了
            //return;
            //}
            #endregion 公転軌道から離脱した後フリーズする原因のため削除
        }
        //加速度が生じている
        else if (accel != 0 && status == playerStatus.free)
        {
            //チリの所有量が一定未満なら
            if (boost.DustCount() < 1)
                return;//何もしない

            //チリ減少カウンターをマイナス
            count -= 1;
            //一定時間たったら
            if (count <= 0)
            {
                //チリの所有量からマイナス１
                boost.AddDust(-1);
                //カウンターを初期化
                count = 60;
            }
        }

        //移動入力取得
        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        #region 【削除】キー入力による移動方向の選定
        //if(isMove==false)
        //{
        //    VectorCheck();
        //    if (vectorCheck.x > 0&&vectorCheck.y>=0)
        //    {
        //        if(isVertical)
        //        {
        //            x = -1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = 1;
        //        }
        //    }
        //    else if (vectorCheck.x <= 0 && vectorCheck.y >= 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = -1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = -1;
        //        }

        //    }
        //    else if (vectorCheck.x <= 0 && vectorCheck.y < 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = 1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = -1;
        //        }
        //    }
        //    else if (vectorCheck.x > 0 && vectorCheck.y >= 0)
        //    {
        //        if (isVertical)
        //        {
        //            x = 1;
        //            y = 0;
        //        }
        //        else
        //        {
        //            x = 0;
        //            y = 1;
        //        }
        //    }
        //}
        #endregion 【削除】キー入力による移動方向の選定

        //入力方向に合わせた方向を取得
        //(ベクトルをノーマライズ)
        getKeyDir = new Vector3(x, y, 0).normalized;
        normalAccel = accel * memAccel;

        #region 削除
        //コースアウト処理中でなく,チリ消費のインターバル期間中でもない
        //if (status != playerStatus.courseOut && !Interval)
        //{
        ////もし移動入力があれば
        //if (getKeyDir != Vector3.zero && (rotation.IsCatch || accel == 0))
        //{
        //慣性のフラグをfalse
        //if (inertia)
        //    inertia = false;
        //入力方向が違ったら移動方向を変更
        //if (moveDir.normalized != getKeyDir &&
        //    getKeyDir.magnitude != 0)
        //moveDir = getKeyDir;
        //移動
        //移動量を確定
        //Velocity = moveDir * normalAccel / 10;
        //実際に移動量分座標に反映
        //transform.position += Velocity;
        ////加速度増加
        //if (accel <= accelInitial + 0.1f)
        //{
        //    accel *= acceleration;
        //    Debug.Log("Hit");
        //}
        //}

        ////もし前フレームの入力した移動量より今の入力した移動量が小さかったら
        //else if (getKeyDir.magnitude < beforeDir.magnitude
        //         && !isBoost
        //         && getKeyDir.magnitude == 0)
        //{
        //    //慣性のフラグtrue
        //    //inertia = true;
        //    //移動量に加速度をかけて
        //    //moveVelocity *= accel;
        //    ////加速度を初期化
        //    //accel = accelInitial;
        //}
        #region 【削除】ほぼ移動していないに等しかったら
        //else if (moveDir.magnitude <= 0.01f)
        //{
        //    //うごうご
        //    //RandomMove();
        //}
        #endregion 【削除】ほぼ移動していないに等しかったら
        //}
        #endregion

        //ブースト状態なら
        if (status == playerStatus.boost)
        {
            #region メソッド化により削除
            ////全く動いていないときにのみ、加速処理を行う
            //if (accel == 0)
            //{
            //    accel = 0.1f;
            //    beforBoostAccel += 0.1f;
            //}

            //if (!boostEnd)//ブーストが終わってなければ
            //{
            //    //現在加速度が加速の上限に達していなければ
            //    if (accel < boostMax)
            //        accel *= boostAccel;//加速度を更に微量追加
            //    //既に加速の上限に達したら
            //    else
            //    {
            //        //加速度を上限に補正
            //        accel = boostMax;
            //        //加速終了フラグをON
            //        boostEnd = true;
            //    }
            //}
            ////加速がすでに終了状態
            //else
            //{
            //    //現在加速度が前フレームより大きい場合
            //    if (accel > beforBoostAccel)
            //        //加速度が大きくなりすぎないように補正
            //        accel /= boostDeceleration;
            //    //現在加速度が前フレームより小さい
            //    else
            //    {
            //        //accel = beforBoostAccel * memAccel;
            //        memAccel += 0.1f;
            //        inertia = true;
            //        isBoost = false;
            //        manager.BoostParticleEnd();
            //    }
            //}
            #endregion メソッド化により削除
            //BoostUpdate();
        }

        if (status == playerStatus.courseOut)
        {
            #region メソッド化により削除
            //if (!coseoutMin)
            //{
            //    accel /= 1.1f;
            //    if (accel <= 0.1f)
            //    {
            //        coseoutMin = true;
            //        moveDir = (SearchPlanet().transform.position - transform.position).normalized;
            //    }
            //}
            //else if (accel < 1)
            //{
            //    accel *= 1.1f;
            //    if (accel > 1)
            //        accel = 1;
            //}
            #endregion
            CourseOutUpdate();
        }

        if (status != playerStatus.courseOut)
        {
            //Velocityの大きさが一定以上なら一定に調整
            if (accel > accelMax)
                accel = accelMax;

            if (accel < accelInitial)
                accel = accelInitial;
        }
        ////実際に移動量分座標に反映
        //transform.position += Velocity;

        if (Interval)
        {
            count -= 1;
            if (count <= 0)
            {
                Interval = false;
            }
        }

        if (status != playerStatus.courseOut)
            moveDir = (moveDir + (getKeyDir / 100)).normalized;

        Velocity = moveDir * accel * memAccel / 10;
        transform.position += Velocity;

        //前フレームの入力方向を今の入力方向に
        beforeDir = getKeyDir;
    }
    void Update()
    {
        vectorCheck = transform.localPosition;
    }
    void VectorCheck()//横か縦か(今は操作に関係してなさそう)
    {
        float vc;
        vc = Mathf.Abs(vectorCheck.x) - Mathf.Abs(vectorCheck.y);
        if (vc >= 0)
        {
            isVertical = false;//縦向きに
        }
        else if (vc < 0)
        {
            isVertical = true;//横向きに
        }

    }

    /// <summary>
    /// うごうご
    /// </summary>
    //private void RandomMove()
    //{
    //    //移動量がなかったら
    //    if (randomVelocity == Vector3.zero)
    //    {
    //        //最大速度と移動量をランダムで設定
    //        randomVelocity = new Vector3(Random.Range(randomMinX, randomMaxX), Random.Range(randomMinY, randomMaxY), 0);
    //        rSpeed = Random.Range(randomSpeedMin, randomSpeedMax);
    //    }

    //    //移動
    //    rMoveVelocity += randomVelocity / 10;
    //    transform.position += rMoveVelocity;
    //    //移動速度が最大速度に達してたら初期化
    //    if (rMoveVelocity.magnitude >= rSpeed)
    //    {
    //        rMoveVelocity = Vector3.zero;
    //        randomVelocity = Vector3.zero;
    //    }
    //}

    /// <summary>
    /// 加速度取得→プロパティに変更
    /// </summary>
    /// <returns></returns>
    public float GetAccel()
    {
        return accel * memAccel;
    }

    /// <summary>
    /// 加速度設定
    /// </summary>
    /// <param name="accel"></param>
    public void SetAccel(float accel)
    {
        this.accel = accel;
    }

    /// <summary>
    /// コースアウト
    /// </summary>
    public void CorseOut()
    {
        status = playerStatus.courseOut;
        manager.BoostParticleEnd();
    }

    /// <summary>
    /// 一番近い惑星検索
    /// コードお借りしました　http://beatdjam.hatenablog.com/entry/2014/10/22/032751
    /// </summary>
    /// <returns></returns>
    GameObject SearchPlanet()
    {
        float tmpDis = 0;           //距離用一時変数
        float nearDis = 0;          //最も近いオブジェクトの距離
                                    //string nearObjName = "";    //オブジェクト名称
        GameObject targetObj = null; //オブジェクト

        //タグ指定されたオブジェクトを配列で取得する
        foreach (GameObject obs in GameObject.FindGameObjectsWithTag("Star"))
        {
            //自身と取得したオブジェクトの距離を取得
            tmpDis = Vector3.Distance(obs.transform.position, transform.position);

            //オブジェクトの距離が近いか、距離0であればオブジェクト名を取得
            //一時変数に距離を格納
            if (nearDis == 0 || nearDis > tmpDis)
            {
                nearDis = tmpDis;
                //nearObjName = obs.name;
                targetObj = obs;
            }

        }
        //最も近かったオブジェクトを返す
        //return GameObject.Find(nearObjName);
        return targetObj;
    }

    /// <summary>
    /// Playerの一瞬の加速
    /// (ぼひゅっと加速)
    /// </summary>
    public void Boost()
    {
        //既に加速状態ならそこで処理終了
        if (status == playerStatus.boost)
            return;

        //前フレームの加速度に現在フレームの加速度を格納
        beforBoostAccel = accel;
        //加速状態にする
        status = playerStatus.boost;
        //慣性OFF
        //inertia = false;
        //加速終了をOFF
        boostEnd = false;
        manager.BoostParticleStart(moveDir.normalized);
    }

    /// <summary>
    /// Playerの移動方向の設定
    /// </summary>
    /// <param name="velocity">実際に設定する移動量</param>
    public void SetMoveVelocity(Vector3 velocity)
    {
        //移動方向を引数から取得
        moveDir = velocity.normalized;
        //Playerが変なとこに行かないようにその時のZ座標は0にする
        moveDir.z = 0;
        //Debug.Log(moveDir.ToString());
    }

    /// <summary>
    /// Playerの移動量のプロパティ
    /// get:移動量を取得する
    /// set:移動量を設定する
    /// </summary>
    public Vector3 Velocity
    {
        get; set;
    }

    /// <summary>
    /// 加速度加算
    /// </summary>
    /// <param name="add"></param>
    public void AddAccel(float add)
    {
        accel += add;
    }

    /// <summary>
    /// コライダー内にオブジェクト侵入した際の処理
    /// </summary>
    /// <param name="hit">接触対象</param>
    private void OnTriggerEnter(Collider hit)
    {
        if (hit.tag == "GravityArea")
        {
            this.hit = hit;
            InGrav = true;
        }
    }

    /// <summary>
    /// コライダーから抜け出した際の処理
    /// </summary>
    /// <param name="other">コライダーに接触したオブジェクト</param>
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Star")
        {
            Interval = true;
            count = 60;
            status = playerStatus.free;
        }

        if (other.tag == "GravityArea")
            InGrav = false;
    }

    public Vector3 GetKeyDir()
    {
        return getKeyDir;
    }

    /// <summary>
    /// Playerが惑星の重力圏内に突入したかどうか
    /// (プロパティ)
    /// </summary>
    private bool InGrav
    {
        get; set;
    }

    /// <summary>
    /// 惑星の重力圏に突入した際の処理
    /// </summary>
    private void GravCatch()
    {
        if (InGrav &&
           !rotation.IsCatch)
        {
            //惑星へひきつけられる挙動
            this.transform.position -=
                ((this.transform.position -
                 hit.transform.position).normalized) / 10 * gravity;
        }
    }

    public bool CanBoost()
    {
        if (status == playerStatus.free)
            return true;

        return false;
    }

    public Vector2 MoveDir()
    {
        return moveDir.normalized;
    }

    /// <summary>
    /// ブースト時のアップデート
    /// </summary>
    private void BoostUpdate()
    {
        //全く動いていないときにのみ、加速処理を行う
        if (accel == 0)
        {
            if (getKeyDir != Vector3.zero)
                moveDir = getKeyDir;

            accel = 0.1f;
            beforBoostAccel += 0.1f;
        }

        if (!boostEnd)//ブーストが終わってなければ
        {
            //現在加速度が加速の上限に達していなければ
            if (accel < boostMax)
                accel *= boostAccel;//加速度を更に微量追加
            //既に加速の上限に達したら
            else
            {
                //加速度を上限に補正
                accel = boostMax;
                //加速終了フラグをON
                boostEnd = true;
            }
        }
        //加速がすでに終了状態
        else
        {
            //現在加速度が前フレームより大きい場合
            if (accel > beforBoostAccel)
                //加速度が大きくなりすぎないように補正
                accel /= boostDeceleration;
            //現在加速度が前フレームより小さい
            else
            {
                //accel = beforBoostAccel * memAccel;
                memAccel += 0.1f;
                //inertia = true;
                status = playerStatus.free;
                manager.BoostParticleEnd();
            }
        }
    }

    /// <summary>
    /// コースアウト時のアップデート
    /// </summary>
    private void CourseOutUpdate()
    {
        if (!coseoutMin)
        {
            accel /= 1.1f;
            if (accel <= 0.1f)
            {
                coseoutMin = true;
                moveDir = (SearchPlanet().transform.position - transform.position).normalized;
            }
        }
        else if (accel < 1)
        {
            accel *= 1.1f;
            if (accel > accelInitial)
                accel = accelInitial;
        }
    }
}
