﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 惑星周りを公転する処理
/// (Playerオブジェクトにアタッチする事)
/// 作成者:翔ちゃん
/// </summary>
public class Rotation : MonoBehaviour
{
    //フィールド
    //Spaceキー(惑星の衛星軌道から離脱)が押されたか？


  //  bool IsSpace;

    //惑星衛星軌道突入時のタイマーが起動状態か？
    bool revoTimeStart;
    //惑星の衛星軌道に突入時に起動するタイマー
    float startRevoTime = 1f;
    private bool intervalStart;//インターバル開始状態か？
    float intervaltime = 60f;//インターバル時間
    private float speeedpenalty = 1f;//公転軌道の制御 
    public float specialgrav = 0.5f;
    private float gravcontrol = 1;
    private float rotationcontroly;
    private float rotationcontrolx;
    private float rotationsolidx;
    private float rotationsolidy;
    private Vector3 nowposition;
    private Vector3 prevposition;
    private Vector3 vectorSelecter;
    private Transform rotationPlanet;
    private Vector3 distanceposition;
    bool isBoost;
    private float vectorreverse;
    //このスクリプトを所有するオブジェクトのコライダー
    Collider hit;
    Collider prevhit;
    // 惑星とPlayerの成す角度（度数値）
    float nDegree = 0.0f;
    private float nDgree180;//角度を180度単位で表すためのもの
    // nDegreeの値をラジアンに変換する(読み取り専用)
    //ちなみに、MathfクラスのDeg2Radメソッドでも代用可能だぞ！
    readonly float nDegreeToRadian = Mathf.PI / 180.0f;
    // nRadianの値を度数法に変換する(読み取り専用)
    //ちなみに、MathfクラスのRad2Degメソッドでも代用可能だ！
    readonly float nRadianToDegree = 180.0f / Mathf.PI;
    //公転運動の速度
    float nSpeed = 0;

    
    //publicアクセシビリティ修飾子のデータ
    [Header("公転時の加速補正値")]
    public float revisionParam = 15f;
    private float distance;//プレイヤーと惑星との距離

    private PlayerParticleManager manager;
    #region 不要になった変数群
    //publicアクセス修飾子のフィールド
    //[Header("楕円運動の水平中心座標")]
    //public float nCenterX = 0.0f;
    //[Header("楕円運動の垂直中心座標")]
    //public float nCenterZ = 0.0f;
    //[Header("楕円運動の水平方向の半径")]
    //public float nRadiusX = 10.0f;
    //[Header("楕円運動の垂直方向の半径")]
    //public float nRadiusZ = 10.0f;

    //重力下にあるかどうか。
    //bool Iscatch;→プロパティに委託し、変数は不要になった

    //float nRadian; // nDegreeの値をラジアンに変換した値
    //float yDegree = 0.0f;
    //float yRadian;

    //public float speedgear = 1;
    //public float radiuscontrol = 20f;

    //float boost = 3;

    //float x;
    //float y;
    //float vx;
    //float vz;
    //float gx;
    //float gz;
    //float rotation;
    //float angle;

    //float test;
    #endregion 不要になった変数群

    // Start is called before the first frame update
    void Start()
    {
        //Spaceキーは非押下状態にする
        //IsSpace = false;
        //惑星の衛星軌道に合流していない状態にする
        ReleasePlanet();
        vectorreverse = 1;
        //コライダー内を空にする
        hit = null;
        prevhit = null;
        //time = 1;//重複代入
        //衛星軌道突入時のタイマーの起動状態をOFFに
        revoTimeStart = false;
        //インターバル関連
        intervalStart = false;
        intervaltime = 60f;
        DettatchParent();
        manager = GetComponent<PlayerParticleManager>();
    }

    /// <summary>
    /// 回転角度関係の変数の初期化
    /// </summary>
    void Initialize()
    {
        ReleasePlanet();
        // 角度を０にする
        nDegree = 0;
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
        //nRadian = 0;
        //このスクリプト所有者の回転を０に戻す
        transform.rotation = Quaternion.identity;
        speeedpenalty=1f;
        gravcontrol = 1f;
       // ReleasePlanet();
    }

    #region 惑星との角度の計算方法が変わったため削除
    //void Set()
    //{
    //    transform.position.Normalize();
    //    nRadiusX = Mathf.Sqrt(((transform.position.x - hit.transform.position.x) * (transform.position.x - hit.transform.position.x)) +
    //       ((transform.position.y - hit.transform.position.y) * (transform.position.y - hit.transform.position.y)));
    //    //プレイヤーと惑星の距離を三平方の定理で計算。そのままだととんでもない数値になるので調整。
    //   // nRadiusX = nRadiusX / radiuscontrol;
    //}
    #endregion 惑星との角度の計算方法が変わったため削除

    /// <summary>
    /// 現在は用途はない状態のようだ
    /// </summary>
    void VectorCheck()
    {
        //if (transform.position.x - hit.transform.position.x >= 0)
        //{ vx = 1; }
        //else { vx = -1; }
        //if (transform.position.z - hit.transform.position.z >= 0)
        //{ vz = 1; }
        //else { vz = -1; }
        //if (x >= 0) { gx = 1; }
        //else { gx = -1; }
        //if (y >= 0) { gz = 1; }
        //else { gz = -1; }
        //if (vx * vz == 1 && gx * gz == -1)
        //{ rotation = 1; }
        //else if (vx * vz == -1 && gx * gz == 1)
        //{ rotation = -1; }
    }
    //ここは今はあんまり意味ないんで無視してください。
    void DistanceSet()
    {
        distance = Vector3.Distance(transform.position, hit.transform.position);
        distanceposition = hit.transform.position - transform.position;
        Debug.Log(DistanceAngle());
        Debug.Log(VectorAngle());
        Debug.Log(VectorAngle()-DistanceAngle());
        if(VectorAngle() - DistanceAngle() >= 0)
        {
            vectorreverse = -1f;
        }
        else
        {
            vectorreverse = 1f;
        }
       // Debug.Log(vectorSelecter.normalized);
    }
    // Update is called once per frame
    void Update()
    {
        
       // Debug.Log(hit.transform.position - transform.position);
        if (transform.parent!=null)
        {
            rotationsolidx = 360-transform.parent.eulerAngles.y;
            rotationsolidy = transform.parent.eulerAngles.x;
            
        }
        nDgree180 = nDegree % 180;//現状の回転角度を180度を、基準として見た場合の余剰角
        nowposition = transform.position;
        vectorSelecter = nowposition - prevposition;
        prevposition = nowposition;
        //インターバル状態なら
        if (Interval)
        {
            //自身の親は設定しない
            //DettatchParent();
            
            //インターバル時間から減算していく
            intervaltime = intervaltime - 1;
        }
        //インターバル時間がなくなった
        if (intervaltime == 0)
        {
            //インターバル状態を解除
            prevhit = null;
            Interval = false;
            
            //インターバル時間を戻す(60がマジックナンバーかな)
            intervaltime = 60f;
        }
        //rotationPlanet = transform.parent;
        //    rotationcontroly = rotationPlanet.GetComponent<Test>().GetRotationY();
        //rotationcontrolx = rotationPlanet.GetComponent<Test>().GetRotationX();
    }

    /// <summary>
    /// コライダー内に他のオブジェクトが侵入した際の処理
    /// (ColliderコンポーネントのIsTriggerにチェックを入れると処理される)
    /// </summary>
    /// <param name="hit">衝突対象</param>
    void OnTriggerEnter(Collider hit)
    {
        //衝突対象の所有タグがStarで、インターバル状態でなくなった
        if (hit.CompareTag("Star")&&hit!=prevhit&&IsCatch==false)
        {
            //自分のコライダーに代入
            this.hit = hit;
            AttatchParent(hit.transform);
            prevhit = hit;
            DistanceSet();
            //衛星軌道突入タイマーを起動させる
            revoTimeStart = true;

        }//惑星の重力圏に入った場合、タイムカウントが開始されます。カウントが終了したらローテンション開始。
        //if(hit.CompareTag("SpecialGravity"))
        //{
        //    Destroy(hit.gameObject);
        //    DownSpeedtoGrav();
        //}

    }
    //void OnTriggerStay(Collider hit)
    //{
    //    //衝突対象の所有タグがStarで、インターバル状態でなくなった
    //    if (hit.CompareTag("Star") && hit != prevhit && IsCatch == false)
    //    {
    //        //自分のコライダーに代入
    //        this.hit = hit;
    //        AttatchParent(hit.transform);
    //        prevhit = hit;
    //        DistanceSet();
    //        //衛星軌道突入タイマーを起動させる
    //        revoTimeStart = true;

    //    }//惑星の重力圏に入った場合、タイムカウントが開始されます。カウントが終了したらローテンション開始。
    //    //if(hit.CompareTag("SpecialGravity"))
    //    //{
    //    //    Destroy(hit.gameObject);
    //    //    DownSpeedtoGrav();
    //    //}

    //}

    /// <summary>
    /// フレーム数に影響せず一定間隔に行われる更新処理
    /// </summary>
    void FixedUpdate()
    {
        
        //Debug.Log(Interval);
        #region キー入力から移動量を算出するのはPlayerMoverに委託
        //x = Input.GetAxis("Horizontal");
        //y = Input.GetAxis("Vertical");
        #endregion キー入力から移動量を算出するのはPlayerMoverに委託

        //スペースキーかFire1ボタンが押された
        if (Input.GetKeyDown(KeyCode.Space)
            || Input.GetButtonDown("Fire2"))
        {
            //惑星の衛星軌道上にいるなら
            if (IsCatch)
            {
                //自身のZ座標の親(惑星)とのZ座標の差分を更に引く
                transform.position =
                    transform.position -
                    new Vector3(0, 0, transform.position.z - transform.parent.position.z);
                Interval = true;
                //スペースキー押下状態にする

                //インターバル状態を有効にする
                
                //自身が保有しているPlayerMoverから、
                //移動量を設定する
                //→これで惑星の衛星軌道上から離脱した後に移動もする
                
                ReleaseFromPlanet();
                manager.BreakawayParticleStart(new Vector3(Mathf.Cos((nDegree * nDegreeToRadian) + 1.5f), Mathf.Sin((nDegree * nDegreeToRadian) + 1.5f), 0).normalized);
                //GetComponent<PlayerMover>().
                //SetMoveVelocity(new Vector3(-transform.localPosition.y*rotationcontroly , transform.localPosition.x * rotationcontrolx, 0)
                //* speeedpenalty);

                //GetComponent<PlayerMover>().
                //SetMoveVelocity(new Vector3(-(transform.position.y - hit.transform.position.y)*rotationcontroly, (transform.position.x - hit.transform.position.x)*rotationcontrolx, (transform.position.z - hit.transform.position.z))
                //* speeedpenalty);
            }
        }
        //スペースキー入力状態なら
        //if (IsSpace)
        //{
        //    //惑星の衛星軌道から離脱する
        //    ReleaseFromPlanet();
        //    //スペースキー押下状態を解消する
        //    IsSpace = false;
        //    #region メソッド化により削除
        //    ////惑星の衛星軌道から離脱状態にする
        //    //IsCatch = false;
        //    ////自身の親をカラにする(惑星との親子関係を解消する)
        //    //transform.parent = null;
        //    ////回転関連の角度を初期化する
        //    //Initialize();
        //    ////スペースキー押下状態を解消する
        //    //IsSpace = false;
        //    #endregion メソッド化により削除
        //}

        #region speedgear変数を使わなくなったため削除
        //if (Iscatch == false)
        //{
        //    //transform.position += new Vector3(x, y, 0) / 5;
        //}
        //if (Input.GetKeyDown(KeyCode.Q))
        //{
        //    speedgear = 0;
        //}
        //if (Input.GetKeyDown(KeyCode.E))
        //{
        //    speedgear = 1;
        //}
        #endregion speedgear変数を使わなくなったため削除

        //衛星軌道突入タイマーが起動状態
        if (revoTimeStart)
        {
            //タイマーを減算していく
            startRevoTime = startRevoTime - 1;
        }
        //タイマーが時間切れ
        if (startRevoTime == 0)
        {
            //惑星の衛星軌道に合流状態にする
            CatchPlanet();

            //直前のPlayerの移動から、回転時の速度を決定
            nSpeed =
                GetComponent<PlayerMover>().
                Velocity.
                magnitude * revisionParam;

            
            //惑星と自身の成す角度を算出する
            nDegree = ColcPlanetAngle();
            transform.Rotate(new Vector3(0, 0, nDegree+180f));
            #region メソッド化により削除
            //Mathf.Atan2(
            //(transform.position.y - hit.transform.position.y),
            //(transform.position.x - hit.transform.position.x))
            //* nRadianToDegree;//弧度法で算出されたものを度数法にする
            #endregion メソッド化により削除

            //衛星軌道突入タイマーを止める
            revoTimeStart = false;
            //タイマーを初期化(1がマジックナンバーかな)
            startRevoTime = 1;
        }//カウント終了後、ローテンション開始。開始前にプレイヤー自身と惑星の距離を計算。度数化。

        #region この例外処理に到達する事がないため削除
        //if (hit == null)
        //{
        //    //惑星の衛星軌道に合流状態なら
        //    if (IsCatch)
        //    {
        //        //離脱状態にする
        //        IsCatch = false;
        //        //角度関係を初期化
        //        Initialize();
        //        Debug.Log("例外処理に到達");
        //    }
        //    return;
        //}
        #endregion この例外処理に到達する事がないため削除

        //惑星の衛星軌道に合流状態なら
        if (IsCatch)
        {
            //公転させる
            
            RotationCheck();
            DynamicRevolution(vectorreverse*nSpeed*speeedpenalty * gravcontrol);
            #region 公転処理をメソッド化したため削除
            ///*timecount++;
            //transform.RotateAround(hit.gameObject.transform.position, Vector3.up, angle * Time.deltaTime * boost);*/

            //// 移動する分の値をnDegreeに入れる
            //nDegree = nDegree + nSpeed;

            ////nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する

            //// nDegreeの値をラジアンに変換した値をnRadianに入れる
            //nRadian = nDegree * nDegreeToRadian;

            //// ここで回転。
            ////親(惑星)からの相対座標を円形に変化させていく
            //transform.localPosition =
            //    new Vector3(
            //        Mathf.Cos(nRadian) * 1,//nRadianが加算され座標が動的に変動していく
            //        Mathf.Sin(nRadian) * 1, 
            //        0);

            ////transform.position += new Vector3(0, Mathf.Sin(yRadian)*nRadiusX,0);
            #endregion 公転処理をメソッド化したため削除
        }
    }

    /// <summary>
    /// 惑星が爆発時の自身の処理
    /// </summary>
    public void Explode()
    {
        //自身の座標に、親(惑星)とのZ座標の差分を更に引く
        transform.position =
            transform.position -
            new Vector3(
                0,
                0,
                transform.position.z - transform.parent.position.z);
        
        //インターバル状態をONにする
        intervalStart = true;
        //惑星との親子関係を解消する
        ReleaseFromPlanet();
        //IsSpace = true;//↑より離脱だと分かる処理に変更
        
    }

    ///// <summary>
    ///// 捕縛状態か？→プロパティ化
    ///// </summary>
    ///// <returns>フィールドIsCatch</returns>
    //public bool IsCatch()
    //{
    //    return Iscatch;
    //}

    /// <summary>
    /// 捕縛状態のプロパティ
    /// (フィールドの変数とメソッドの役割をこれで兼任する)
    /// get:外部からbool値を取得可能
    /// set:内部からしか設定不可
    /// </summary>
    public bool IsCatch
    {
        get;
        private set;
    }

    private void CatchPlanet()
    {
        IsCatch = true;
        Debug.Log("CatchPlanet");
    }
    private void ReleasePlanet()
    {
        IsCatch = false;
        Debug.Log("ReleasePlanet");
    }
    public bool Interval
    {
        get { return intervalStart; }
        set { intervalStart = value; }
    }

    /// <summary>
    /// 角度が変わっていく衛星の公転運動
    /// </summary>
    /// <param name="angleDeg">公転角度の変化量(度数法)</param>
    private void DynamicRevolution(float angleDeg)
    {
        // 移動する分の値をnDegreeに入れる
        nDegree = nDegree + angleDeg;
        // nDegreeの値をラジアンに変換した値をnRadianに入れる
        var nRadian = nDegree * nDegreeToRadian;

        // ここで回転。
        //親(惑星)からの相対座標を円形に変化させていく
        transform.localPosition =
            new Vector3(
                Mathf.Cos(nRadian) * 1.25f,//nRadianが加算され座標が動的に変動していく
                Mathf.Sin(nRadian) * 1.25f,
                0);
        Debug.Log("DynamicRevolution");
        //transform.Rotate(new Vector3(0, 0,angleDeg));
    }
    private float DistanceAngle()
    {
        //惑星と自身の座標の差分から、
        //X,Y座標で逆三角関数(タンジェント)を使用する
        //これにより惑星と自身の成す角度を算出する
        return
            Mathf.Atan2(
            distanceposition.y,distanceposition.x)
            * nRadianToDegree;//弧度法で算出されたものを度数法にする
    }

    /// <summary>
    /// 惑星とPlayerがなす角度を算出
    /// </summary>
    /// <returns>角度(度数法)</returns>
    private float ColcPlanetAngle()
    {
        //惑星と自身の座標の差分から、
        //X,Y座標で逆三角関数(タンジェント)を使用する
        //これにより惑星と自身の成す角度を算出する
        return
            Mathf.Atan2(
            (transform.position.y - hit.transform.position.y),
            (transform.position.x - hit.transform.position.x))
            * nRadianToDegree;//弧度法で算出されたものを度数法にする
    }
    private float VectorAngle()
    {
        return
            Mathf.Atan2(vectorSelecter.y, vectorSelecter.x) * nRadianToDegree;
    }

    ///// <summary>
    ///// 捕縛状態か？→プロパティ化
    ///// </summary>
    ///// <returns>フィールドIsCatch</returns>
    //public bool IsCatch()
    //{
    //    return Iscatch;
    //}

    /// <summary>
    /// 惑星と自身の親子関係(惑星の衛星軌道に合流している状態)を解除する
    /// </summary>
    private void ReleaseFromPlanet()
    {
        GetComponent<PlayerMover>().
                    SetAccel(speeedpenalty * nSpeed * 0.5f);
        GetComponent<PlayerMover>().
            SetMoveVelocity
            (new Vector3(Mathf.Cos((nDegree * nDegreeToRadian) + (1.5f*vectorreverse)) * Mathf.Cos((rotationsolidx * nDegreeToRadian)),
            Mathf.Sin((nDegree * nDegreeToRadian) + (1.5f*vectorreverse)) * Mathf.Cos((rotationsolidy * nDegreeToRadian)),
            0));
        //惑星の衛星軌道から離脱状態にする
        // ReleasePlanet();
        //自身の親をカラにする(惑星との親子関係を解消する)
        DettatchParent();
        //回転関連の角度を初期化する
        Initialize();
    }

    private void RotationCheck()
    {
        speeedpenalty = 0.5f+Mathf.Abs(transform.position.z)/distance;
    }
    public float SpeedPenalty()
    {
        return speeedpenalty;
    }
    
    public void DownSpeedtoGrav()
    {
        gravcontrol = specialgrav;
    }

    public void SpeedUp()
    {
        nSpeed += 0.5f;
        if (nSpeed < 6f)
        {
            manager.BreakawayParticleStart(new Vector3(Mathf.Cos((nDegree * nDegreeToRadian) + 1.5f), Mathf.Sin((nDegree * nDegreeToRadian) + 1.5f), 0).normalized);
        }
        if (nSpeed>6f)
        {
            nSpeed = 6f;
        }
        
    }
    private void AttatchParent(Transform parent)
    {
        transform.parent = parent;
    }

    private void DettatchParent()
    {
        transform.parent = null;
        //Debug.Log("DettachParent");
    }

}
