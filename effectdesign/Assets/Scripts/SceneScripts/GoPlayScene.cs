﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// ゲームプレイシーンへ遷移
/// 作成者:
/// </summary>
public class GoPlayScene : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
