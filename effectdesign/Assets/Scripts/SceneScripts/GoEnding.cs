﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// エンディングへ遷移
/// 作成者:
/// </summary>
public class GoEnding : MonoBehaviour
{
    [SerializeField] GameObject player;
    private Boost1 boost;
    private void Start()
    {
        boost = player.GetComponent<Boost1>();
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(boost.IsGameOver().ToString());
        if (boost.IsGameOver()) 
        {
            SceneManager.LoadScene("Ending");
        }
    }
}
